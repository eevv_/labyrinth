# labyrinth
Written in C.
Game saves when you close it to `save.bin`.
## Controls
Move with standard wasd.
Press `e` to get the layout of the labyrinth to stdout.
# Dependencies
* `SDL2`
# Credits
* [Decay (music, permitted)](https://d3cay.bandcamp.com/album/metamilk-score)
