/*
player.h
-> player definition
*/

#pragma once

#include <stdbool.h>
#include <stdlib.h>
#include "bytes.h"
#include "vec2f.h"

typedef struct {
	Vec2f pos;
	float dir;
} Player;
