#include <stdbool.h>
#include "pix.h"

/* round down */
Vec3f low(Vec3f v) {
	v.x *= 8.0f;
	v.y *= 8.0f;
	v.z *= 4.0f;

	v.x = floorf(v.x) / 8.0f;
	v.y = floorf(v.y) / 8.0f;
	v.z = floorf(v.z) / 4.0f;

	return v;
}

/* round up */
Vec3f high(Vec3f v) {
	v.x *= 8.0f;
	v.y *= 8.0f;
	v.z *= 4.0f;

	v.x = ceilf(v.x) / 8.0f;
	v.y = ceilf(v.y) / 8.0f;
	v.z = ceilf(v.z) / 4.0f;

	return v;
}

Uint32 pix_8bit(Vec3f v) {
	v = high(v);
	return pix_from_vec3f(v);
}

Uint32 pix_from(int r, int g, int b) {
	return 0xff000000 | (r << 16) | (g << 8) | b;
}

Uint32 pix_from_vec3f(Vec3f v) {
	v = vec3f_scale(v, 255.0f);
	return pix_from(v.x, v.y, v.z);
}

/* TODO make dither_matrix column major */
static bool dither_matrix[2 * 2 * 5] = {
	/* 0 */
	0, 0,
	0, 0,
	/* 1 */
	0, 0,
	0, 1,
	/* 2 */
	0, 1, /* there is a hidden binary counting here */
	1, 0, /* every 2nd one */
	/* 3 */
	0, 1,
	1, 1,
	/* 4 */
	1, 1,
	1, 1,
};

/* ?TODO? figure out formula for no lookup table */
bool dither(Vec2i p, float n) {
	n *= 4.0f;
	int ni = n;
	return dither_matrix[p.x + p.y * 2 + ni * 4];
}

Uint32 pix_dither(Vec3f v, Vec2i p) {
	p.x = p.x % 2;
	p.y = p.y % 2;
	
	Vec3f vl = low(v);
	Vec3f vh = high(v);
	
	float n = vec3f_inv_interp(vl, vh, v);
	v = (dither(p, n)) ? vh : vl;
	
	return pix_from_vec3f(v);
}
