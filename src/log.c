#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "cfg.h"
#include "log.h"

static FILE* log_file;
static bool log_to_file;

static void log_common(const char* type) {
	time_t now = time(NULL);
	struct tm* now_tm = localtime(&now);
	char time_str[9];
	strftime(time_str, 9, "%H:%M:%S", now_tm);
	fputs(time_str, stdout);
	fputc(' ', stdout);
	fputs(type, stdout);
	fputc(' ', stdout);

#if CFG_LOG_TO_FILE
	if (log_to_file) {
		fputs(time_str, log_file);
		fputc(' ', log_file);
		fputs(type, log_file);
		fputc(' ', log_file);
	}
#endif
}

void log_err(const char* format, ...) {
	log_common("log/err");
	
	va_list args;
	va_start(args, format);
	vprintf(format, args);
	va_end(args);
	putc('\n', stdout);

#if CFG_LOG_TO_FILE
	if (log_to_file) {
		va_start(args, format);
		vfprintf(log_file, format, args);
		va_end(args);
		putc('\n', log_file);
	}
#endif
	
	exit(1);
}

void log_free(void) {
	if (!log_to_file) {
		return;
	}
	
	fclose(log_file);
}

void log_init(void) {
#if CFG_LOG_TO_FILE
	log_to_file = true;
	
	log_file = fopen("log.txt", "w");
	if (!log_file) {
		log_to_file = false;
		log_err("failed to open \"log.txt\"");
		return;
	}
#endif
}

void log_ok(const char* format, ...) {
	log_common("log/ok");
	
	va_list args;
	va_start(args, format);
	vprintf(format, args);
	va_end(args);
	putc('\n', stdout);

#if CFG_LOG_TO_FILE
	if (log_to_file) {
		va_start(args, format);
		vfprintf(log_file, format, args);
		va_end(args);
		putc('\n', log_file);
	}
#endif
}
