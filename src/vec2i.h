/*
vec2i.h
-> handles things relating
   to 2 dimensional integer
   vectors
*/

#pragma once

typedef struct {
	int x;
	int y;
} Vec2i;

Vec2i vec2i_add(Vec2i v0, Vec2i v1);
Vec2i vec2i_clamp(Vec2i v, float s, float e);
Vec2i vec2i_div(Vec2i v, int d);
Vec2i vec2i_from(int x, int y);
Vec2i vec2i_sub(Vec2i v0, Vec2i v1);
