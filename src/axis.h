/*
axis.h
-> handling axis
-> axis to vector, etc
-> used in maps, ents, tracing
*/

#pragma once

#include <stdbool.h>
#include "vec2f.h"

typedef enum {
	AXIS_PX,
	AXIS_PY,
	AXIS_NX,
	AXIS_NY
} Axis;

bool axis_is_x(Axis axis);
bool axis_is_y(Axis axis);
Axis axis_rot90(Axis axis);
Vec2f axis_to_vec2f(Axis axis);
