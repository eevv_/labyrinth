#include <assert.h>
#include "deser.h"
#include "ent.h"
#include "ser.h"

Vec2f ent_door_dir(const Ent* ent) {
	assert(ent->type == ENT_DOOR);
	
	return axis_to_vec2f(axis_rot90(ent->axis));
}

/* based on line intersections */
EntTrace ent_trace(const Ent* ent, Vec2f pos, Vec2f dir) {
	Vec2f ent_dir;
	if (ent->type == ENT_DOOR) {
		/* doors have their own direction */
		ent_dir = ent_door_dir(ent);
	} else {
		/* key and exit always face the origin pos */
		ent_dir = vec2f_sub(pos, ent->pos);
		ent_dir = vec2f_rot90(ent_dir);
		ent_dir = vec2f_unit(ent_dir);
	}
	
	float cross = vec2f_cross(ent_dir, dir);
	EntTrace trace;
	if (cross == 0.0f) {
		trace.hit = false;
		return trace;
	}
	
	float ent_dist = (vec2f_cross(pos, dir) - vec2f_cross(ent->pos, dir)) / cross;
	if (ent_dist < -0.5f || ent_dist > 0.5f) {
		trace.hit = false;
		return trace;
	}
	
	float dist = (vec2f_cross(ent->pos, ent_dir) - vec2f_cross(pos, ent_dir)) / -cross;
	if (dist < 0.0f) {
		trace.hit = false;
		return trace;
	}
	
	trace.hit = true;
	trace.ent_type = ent->type;
	trace.x = ent_dist;
	trace.dist = dist;
	
	return trace;
}

void ents_free(Ents* ents) {
	free(ents->ents);
}

Ent* ents_get_ptr(const Ents* ents, int i) {
	return ents->ents + i;
}

void ents_init(Ents* ents, size_t size) {
	ents->size = size;
	ents->ents = malloc(size * sizeof(Ent));
}

EntsTrace ents_trace(const Ents* ents, Vec2f pos, Vec2f dir) {
	EntsTrace trace;
	trace.size = ents->size;
	
	for (int i = 0; i < ents->size; i += 1) {
		trace.traces[i] = ent_trace(ents->ents + i, pos, dir);
	}
	
	/* sort by distance (furthest to closest) */
	/* doesn't matter what sorting algorithm */
	/* we use here because there will never */
	/* be many entities */
	/* the reason for sorting is so that */
	/* transparency works properly when */
	/* rendering */
	for (int i = 0; i < ents->size; i += 1) {
		int max = i;
		for (int j = i; j < ents->size; j += 1) {
			if (trace.traces[j].dist > trace.traces[max].dist) {
				max = j;
			}
		}
		/* swap */
		EntTrace temp = trace.traces[i];
		trace.traces[i] = trace.traces[max];
		trace.traces[max] = temp;
	}
	
	return trace;
}
