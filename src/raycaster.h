/*
raycaster.h
-> handles drawing the scene to the screen
*/

#pragma once

#include "map.h"
#include "player.h"
#include "scene.h"
#include "tex.h"
#include "win.h"

typedef struct {
	Texs texs;
} Raycaster;

void raycaster_free(Raycaster* raycaster);
/* place where texture loading happens, etc... */
void raycaster_init(Raycaster* raycaster);
void raycaster_render(Raycaster* raycaster, Win* win, const Scene* scene);
