/*
ser.h
-> handles serialization
   of various types
*/

#pragma once

#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
#include "bytes.h"
#include "scene.h"

bool ser_ent(Ent ent, size_t* pos, Bytes* bytes);
bool ser_ents(Ents ents, size_t* pos, Bytes* bytes);
bool ser_float(float flt, size_t* pos, Bytes* bytes);
bool ser_map(Map map, size_t* pos, Bytes* bytes);
bool ser_player(Player player, size_t* pos, Bytes* bytes);
bool ser_scene(Scene scene, size_t* pos, Bytes* bytes);
bool ser_uint8(uint8_t uint8, size_t* pos, Bytes* bytes);
bool ser_uint16(uint16_t uint16, size_t* pos, Bytes* bytes);
bool ser_uint32(uint32_t uint32, size_t* pos, Bytes* bytes);
bool ser_uint64(uint64_t uint64, size_t* pos, Bytes* bytes);
bool ser_vec2f(Vec2f vec2f, size_t* pos, Bytes* bytes);
