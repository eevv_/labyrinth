/*
bytes.h
-> provides basic byte array IO
-> used in serialization (ser.*)
-> used in deserialization (deser.*)
*/

#pragma once

#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>

typedef struct {
	size_t size;
	uint8_t* bytes;
} Bytes;

void bytes_free(Bytes* bytes);
bool bytes_from_path(Bytes* bytes, const char* path);
void bytes_init(Bytes* bytes, size_t size);
bool bytes_to_path(const Bytes* bytes, const char* path);
