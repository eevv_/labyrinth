#include <assert.h>
#include "farb.h"
#include "log.h"
#include "tex.h"

void tex_free(Tex* tex) {
	/* cast because it doesn't modify the data */
	free((Vec3f*) tex->colors);
}

static void from_farb(Tex* tex, const Farb* farb) {
	tex_init(tex, farb->width, farb->height);
	
	for (int x = 0; x < tex->w; x += 1) {
		for (int y = 0; y < tex->h; y += 1) {
			uint64_t pix = farb_get(farb, vec2i_from(x, y));
			Vec3f color;
			color.x = (pix >> 48) & 0xffff;
			color.y = (pix >> 32) & 0xffff;
			color.z = (pix >> 16) & 0xffff;
			color = vec3f_scale(color, 1.0f / 0xffff);
			tex_set(tex, vec2i_from(x, y), color);
		}
	}
}

bool tex_from_path(Tex* tex, const char* path) {
	log_ok("-> (texture) \"%s\"", path);
	
	Farb farb;
	if (!farb_from_path(&farb, path)) {
		return false;
	}
	
	from_farb(tex, &farb);
	farb_free(&farb);
	
	return true;
}

Vec3f tex_get(const Tex* tex, Vec2i pos) {
	assert(pos.x >= 0 && pos.x <= tex->w);
	assert(pos.y >= 0 && pos.y <= tex->h);
	
	return tex->colors[pos.y + pos.x * tex->h];
}

void tex_init(Tex* tex, int w, int h) {
	tex->w = w;
	tex->wf = w;

	tex->h = h;
	tex->hf = h;
	
	tex->colors = malloc(w * h * sizeof(Vec3f));
}

static int wrap(int v, int m) {
	v %= m;
	if (v < 0) {
		v = -v;
	}
	return v;
}

Vec3f tex_sample(const Tex* tex, Vec2f p) {
	p.x *= tex->wf;
	p.y *= tex->hf;
	
	Vec2i pos;
	pos.x = p.x;
	pos.y = p.y;
	pos.x = wrap(pos.x, tex->w);
	pos.y = wrap(pos.y, tex->h);
	
	return tex_get(tex, pos);
}

void tex_set(Tex* tex, Vec2i pos, Vec3f color) {
	assert(pos.x >= 0 && pos.x < tex->w);
	assert(pos.y >= 0 && pos.y < tex->h);
	
	tex->colors[pos.y + pos.x * tex->h] = color;
}

void texs_free(Texs* texs) {
	free(texs->texs);
}

int texs_from_paths(Texs* texs, size_t size, const char** paths) {
	texs->texs = malloc(size * sizeof(Tex));
	
	for (int i = 0; i < size; i += 1) {
		if (!tex_from_path(texs->texs + i, paths[i])) {
			return i;
		}
	}
	
	texs->size = size;
	
	return -1;
}

Tex* texs_get_ptr(Texs* texs, int i) {
	assert(i >= 0 && i < texs->size);
	
	return texs->texs + i;
}
