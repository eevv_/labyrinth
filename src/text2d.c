#include <stdlib.h>
#include <string.h>
#include "text2d.h"
#include "pix.h"

void text2d_free(Text2D* text) {
	free(text->text);
}

Text2D text2d_from(int scale, Pnt pnt, const char* text) {
	Text2D r = {scale, pnt, 0, NULL};
	text2d_set_text(&r, text);
	return r;
}

void text2d_render(const Text2D* text2d, Win* win, const Tex* tex) {
	/* if there is no text */
	if (!text2d->size) {
		return;
	}
	
	Vec2i dim = vec2i_from(win->width, win->height);
	
	Vec2i pnt = pnt_to_vec2i(text2d->pnt, dim);
	
	Vec2i len = vec2i_from(text2d->size * text2d->scale, text2d->scale);
	
	Vec3f color;
	/* TODO clamp the coordinates */
	float size_x_chr = (float) len.x / text2d->size;
	for (int x = 0; x <= len.x; x += 1) {
		int index = (text2d->size * x) / len.x;
		char chr = text2d->text[index];
		int chr_x = chr % 16;
		int chr_y = chr /= 16;
		
		Vec2f offset;
		offset.x = fmod(x, size_x_chr);
		offset.x /= size_x_chr;
		for (int y = 0; y <= len.y; y += 1) {
			Vec2f tex_coord;
			tex_coord.x = chr_x;
			tex_coord.y = chr_y;
			
			offset.y = (float) y / (len.y + 1);
			
			tex_coord = vec2f_add(tex_coord, offset);
			tex_coord = vec2f_scale(tex_coord, 1.0f / 16.0f);
			
			color = tex_sample(tex, tex_coord);
			if (color.x + color.y + color.z == 0.0f) {
				continue;
			}
			win_pix_set(win, vec2i_from(x + pnt.x, y + pnt.y), pix_from_vec3f(color));
		}
	}
}

void text2d_set_text(Text2D* text2d, const char* text) {
	free(text2d->text);
	if (text) {
		size_t size = strlen(text);
		text2d->size = size;
		size += 1;
		text2d->text = malloc(size * sizeof(char));
		strcpy(text2d->text, text);
	} else {
		text2d->size = 0;
		text2d->text = NULL;
	}
}
