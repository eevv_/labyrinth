/*
text2d.h
-> handles 2d text
-> rendering of 2d text
-> changing the text
-> used in the UI (ui.*)
*/

#pragma once

#include "pnt.h"
#include "scene.h"
#include "tex.h"
#include "win.h"

typedef struct {
	int scale;
	Pnt pnt;
	/* size of the text */
	/* in chars */
	size_t size;
	char* text;
} Text2D;

void text2d_free(Text2D* text2d);
Text2D text2d_from(int scale, Pnt tl, const char* text);
void text2d_render(const Text2D* text2d, Win* win, const Tex* font);
void text2d_set_text(Text2D* text2d, const char* text);

typedef struct {
	size_t size;
	Text2D* text2ds;
} Text2Ds;
