/*
vec2f.h
-> handles things relating to
   2 dimensional float vectors
*/

#pragma once

typedef struct {
	float x;
	float y;
} Vec2f;

Vec2f vec2f_add(Vec2f v0, Vec2f v1);
float vec2f_cross(Vec2f v0, Vec2f v1);
float vec2f_dist(Vec2f v);
/* AABB max distance */
float vec2f_dist_max(Vec2f v);
/* AABB min distance */
float vec2f_dist_min(Vec2f v);
float vec2f_dot(Vec2f v0, Vec2f v1);
Vec2f vec2f_from(float x, float y);
Vec2f vec2f_from_dir(float dir);
/* interpolation */
Vec2f vec2f_interp(Vec2f v0, Vec2f v1, float a1);
Vec2f vec2f_inv(Vec2f v);
Vec2f vec2f_rot90(Vec2f v);
Vec2f vec2f_scale(Vec2f v, float s);
Vec2f vec2f_sub(Vec2f v0, Vec2f v1);
/* unit vector of v */
Vec2f vec2f_unit(Vec2f v);
