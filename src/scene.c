#include <assert.h>
#include <math.h>
#include "cfg.h"
#include "deser.h"
#include "log.h"
#include "scene.h"
#include "ser.h"

void scene_free(Scene* scene) {
	map_free(&scene->map);
	ents_free(&scene->ents);
}

void scene_fwrite(const Scene* scene, FILE* file) {
	Vec2i ent_posis[4];
	ent_posis[0].x = scene->player.pos.x;
	ent_posis[0].y = scene->player.pos.y;
	for (int i = 1; i < 4; i += 1) {
		Vec2f posf = ents_get_ptr(&scene->ents, i - 1)->pos;
		ent_posis[i].x = posf.x;
		ent_posis[i].y = posf.y;
	}
	
	for (int y = -1; y <= scene->map.height; y += 1) {
		for (int x = -1; x <= scene->map.width; x += 1) {
			/* 0 -> none */
			/* 1 -> player */
			/* 2 -> exit */
			/* 3 -> key */
			/* 4 -> door */
			int ent = 0;
			for (int i = 0; i < scene->ents.size + 1; i += 1) {
				Vec2i ent_pos = ent_posis[i];
				if (x == ent_pos.x && y == ent_pos.y) {
					ent = i + 1;
					break;
				}
			}
			
			const char* str = NULL;
			if (ent) {
				switch (ent) {
					case 1:
						str = "PP";
						break;
					case 2:
						str = "EE";
						break;
					case 3:
						str = "DD";
						break;
					case 4:
						str = "KK";
						break;
					default:
						assert(false);
						break;
				}
			} else {
				Tile tile = map_get(&scene->map, vec2i_from(x, y));
				str = tile == TILE_FLOOR ? "  " : "##";
			}
			
			fputs(str, file);
		}
		
		fputc('\n', file);
	}
}

/* for nicer assert error output */
static bool is_odd(int n) {
	if (n <= 0) {
		return false;
	}

	return n % 2 == 1;
}

void scene_init(Scene* scene, int width, int height) {
	log_ok("initializing scene %dx%d", width, height);
	/* if (width, height) is entered as even */
	/* the maze gen generate a maze that is */
	/* (odd, odd) anyways */
	assert(is_odd(width));
	assert(is_odd(height));
	
	/* init player */
	log_ok("initializing player");
	scene->player.pos.x = 0.5f;
	scene->player.pos.y = 0.5f;
	scene->player.dir = M_PI / 4.0;
	
	/* init map */
	log_ok("initializing map");
	map_init(&scene->map, width, height);
	map_gen(&scene->map);
	
	/* init entities */
	log_ok("initializing entities");
	ents_init(&scene->ents, 3);
	
	Ent* ent;
	
	log_ok("-> exit");
	ent = ents_get_ptr(&scene->ents, 0);
	ent->type = ENT_EXIT;
	ent->pos = vec2f_from(width - 0.5f, height - 0.5f);
	
	size_t path_size;
	Vec2i* path = map_path(&scene->map, vec2i_from(0, 0), vec2i_from(width - 1, height - 1), &path_size);
	size_t half_path = path_size / 2;
	
	log_ok("-> door");
	/* 0 = none found */
	/* 1 = x axis */
	/* 2 = y axis */
	Vec2i door_pos;
	int axis = 0;
	for (size_t i = half_path; i < path_size; i += 1) {
		door_pos = path[i];
		
		Vec2i dirs[4] = {
			vec2i_add(door_pos, vec2i_from(1, 0)),
			vec2i_add(door_pos, vec2i_from(0, 1)),
			vec2i_add(door_pos, vec2i_from(-1, 0)),
			vec2i_add(door_pos, vec2i_from(0, -1))
		};
		
		for (int j = 0; j < 2; j += 1) {
			bool place = map_get(&scene->map, dirs[j + 2]) == TILE_WALL;
			place = place && map_get(&scene->map, dirs[j]) == TILE_WALL;
			if (place) {
				/* TODO j -> j + 2 */
				axis = j;
				goto outer_exit;
			}
		}
	}
	assert(false);
	outer_exit:
	ent = ents_get_ptr(&scene->ents, 1);
	ent->type = ENT_DOOR;
	ent->pos = vec2f_from(door_pos.x + 0.5f, door_pos.y + 0.5f);
	/* 1 -> axis PY */
	/* 2 -> axis PX */
	ent->axis = axis - 1;
	
	log_ok("-> key");
	/* close the door */
	/* to find all possible key locations */
	map_set(&scene->map, door_pos, TILE_WALL);
	size_t fill_size;
	Vec2i* fill = map_fill(&scene->map, vec2i_from(0, 0), &fill_size);
	/* open the door */
	/* so the player can walk through it */
	/* again */
	map_set(&scene->map, door_pos, TILE_FLOOR);
	Vec2i key_pos = fill[rand() % fill_size];
	
	ent = ents_get_ptr(&scene->ents, 2);
	ent->type = ENT_KEY;
	ent->pos = vec2f_from(key_pos.x + 0.5f, key_pos.y + 0.5f);
}

static void handle_ent(Scene* scene, int ent_ind) {
	if (ent_ind == 2) {
		if (scene->ents.size == 3) {
			scene->ents.size -= 1;
			scene->evnt = EVNT_KEY;
			log_ok("got key");
		}
		
		return;
	}
	
	if (ent_ind == 1) {
		if (scene->ents.size == 2) {
			scene->ents.size -= 1;
			scene->evnt = EVNT_DOOR;
			log_ok("got door");
		}
		
		return;
	}
	
	if (ent_ind == 0) {
		if (scene->ents.size == 1) {
			scene->ents.size -= 1;
			scene->evnt = EVNT_EXIT;
			log_ok("got exit");
		}
	}
}

static void handle_ents(Scene* scene) {
	for (int i = 0; i < scene->ents.size; i += 1) {
		Vec2f ent_pos = ents_get_ptr(&scene->ents, i)->pos;
		float dist = vec2f_dist_max(vec2f_sub(scene->player.pos, ent_pos));
		if (dist <= 0.5f) {
			handle_ent(scene, i);
		}
	}
}

static const float padding = 0.1f;
/* padding detection */
static const float padding_d = 0.1f - CFG_EPSILON;

static void scene_move_player(Scene* scene, Vec2f move) {
	MapTrace trace = map_trace(&scene->map, scene->player.pos, move);
	Vec2f new_pos = vec2f_add(scene->player.pos, move);
	Vec2f diff = vec2f_sub(new_pos, trace.pos);
	bool collision = false;
	switch (trace.axis) {
	case AXIS_PX:
		if (diff.x < padding_d) {
			new_pos.x = trace.pos.x + padding;
			collision = true;
		}
		break;
	case AXIS_PY:
		if (diff.y < padding_d) {
			new_pos.y = trace.pos.y + padding;
			collision = true;
		}
		break;
	case AXIS_NX:
		if (diff.x > -padding_d) {
			new_pos.x = trace.pos.x - padding;
			collision = true;
		}
		break;
	case AXIS_NY:
		if (diff.y > -padding_d) {
			new_pos.y = trace.pos.y - padding;
			collision = true;
		}
		break;
	}
	
	if (scene->ents.size == 3) {
		Ent* ent_door = ents_get_ptr(&scene->ents, 1);
		EntTrace trace = ent_trace(ent_door, scene->player.pos, move);
		if (trace.hit) {
			Vec2f ent_pos = vec2f_scale(move, trace.dist);
			ent_pos = vec2f_add(scene->player.pos, ent_pos);
			Vec2f diff = vec2f_sub(new_pos, ent_pos);
			Vec2f ent_dir = ent_door_dir(ent_door);
			ent_dir = vec2f_scale(ent_dir, 0.1f);
			Axis axis;
			if (axis_is_x(ent_door->axis)) {
				axis = move.x > 0.0f ? AXIS_NX : AXIS_PX;
			} else {
				axis = move.y > 0.0f ? AXIS_NY : AXIS_PY;
			}
			Vec2f new_pos_ent = new_pos;
			switch (axis) {
			case AXIS_PX:
				if (diff.x < padding_d) {
					new_pos_ent.x = ent_pos.x + padding;
					collision = true;
				}
				break;
			case AXIS_PY:
				if (diff.y < padding_d) {
					new_pos_ent.y = ent_pos.y + padding;
					collision = true;
				}
				break;
			case AXIS_NX:
				if (diff.x > -padding_d) {
					new_pos_ent.x = ent_pos.x - padding;
					collision = true;
				}
				break;
			case AXIS_NY:
				if (diff.y > -padding_d) {
					new_pos_ent.y = ent_pos.y - padding;
					collision = true;
				}
				break;
			}
			
			float dist_to_wall = vec2f_dist(vec2f_sub(scene->player.pos, new_pos));
			float dist_to_door = vec2f_dist(vec2f_sub(scene->player.pos, new_pos_ent));
			if (dist_to_wall > dist_to_door) {
				new_pos = new_pos_ent;
			}
		}
	}
	
	if (!collision) {
		scene->player.pos = new_pos;
		return;
	}
	
	move = vec2f_sub(new_pos, scene->player.pos);
	scene_move_player(scene, move);
}

/* FIXME the collision detection is not enough */
/* I found 2 bugs: */
/* (1) if you reverse walk into a corner there is colliison fighting */
/*     which makes the screen shake (increment of 0.1) */
/* (2) if you walk into the edge of the door following the wall */
/*     you can clip through the door */
/* conclusion: collision resolving has to be recursive */
/* sliding across the wall has to be done in 2 stages: */
/* (1) move the player in front of the wall (with some padding) */
/* (2) move the player parallel to the wall */
/* after every stage here we have to apply collision detection recursively */

void scene_update(Scene* scene, float dt, const Input* input) {
	scene->evnt = EVNT_NONE;
	
	handle_ents(scene);
	
	/* move front or back */
	if (input->front || input->back) {
		/* TODO use `input->offset_dir` */
		float move_dir = scene->player.dir;
		if (input->back) {
			move_dir = M_PI + move_dir;
		}
		Vec2f move = vec2f_scale(vec2f_from_dir(move_dir), 0.1f);
		scene_move_player(scene, move);
		
		/*	
		*/
	}
	
	if (input->right) {
		scene->player.dir -= 2.0f * dt;
	}
	if (input->left) {
		scene->player.dir += 2.0f * dt;
	}
}

