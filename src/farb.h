/*
farb.h
-> basic farbfeld implementation
-> https://tools.suckless.org/farbfeld/
*/ 

#pragma once

#include <stdint.h>
#include "bytes.h"
#include "vec2i.h"

typedef struct {
	uint32_t width;
	uint32_t height;
	/* RGBA */
	uint64_t* pixs;
} Farb;

void farb_free(Farb* farb);
bool farb_from_path(Farb* farb, const char* path);
uint64_t farb_get(const Farb* farb, Vec2i pos);
void farb_init(Farb* farb, uint32_t width, uint32_t height);
