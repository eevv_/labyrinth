/*
win.h
-> creates a basic window
   interface
*/

#pragma once

#include <SDL2/SDL.h>
#include "vec2i.h"

typedef struct {
	SDL_Window* sdl_win;
	SDL_Surface* sdl_surf;
	int width;
	int height;
	float aspect;
} Win;

void win_free(Win* win);
void win_init(Win* win, const char* title, int width, int height);
void win_pix_set(Win* win, Vec2i pos, Uint32 color);
void win_update(Win* win);
