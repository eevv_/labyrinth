/*
vec3f.h
-> handles things relating to
   3 dimensional float vectors
-> usually used for color
*/

#pragma once

#include <SDL2/SDL.h>

typedef struct {
	float x;
	float y;
	float z;
} Vec3f;

Vec3f vec3f_clamp(Vec3f v, float s, float e);
Vec3f vec3f_from(float x, float y, float z);
float vec3f_inv_interp(Vec3f s, Vec3f e, Vec3f p);
Vec3f vec3f_scale(Vec3f v, float s);
