#include <math.h>
#include "vec2f.h"

Vec2f vec2f_add(Vec2f v0, Vec2f v1) {
	Vec2f r = {v0.x + v1.x, v0.y + v1.y};
	return r;
}

float vec2f_cross(Vec2f v0, Vec2f v1) {
	return v0.x * v1.y - v0.y * v1.x;
}

float vec2f_dist(Vec2f v) {
	return sqrtf(vec2f_dot(v, v));
}

static float max(float x, float y) {
	return (x > y) ? x : y;
}

float vec2f_dist_max(Vec2f v) {
	return max(fabsf(v.x), fabsf(v.y));
}

static float min(float x, float y) {
	return (x > y) ? y : x;
}

float vec2f_dist_min(Vec2f v) {
	return min(fabsf(v.x), fabsf(v.y));
}

float vec2f_dot(Vec2f v0, Vec2f v1) {
	return v0.x * v1.x + v0.y * v1.y;
}

Vec2f vec2f_from(float x, float y) {
	Vec2f r = {x, y};
	return r;
}

Vec2f vec2f_from_dir(float dir) {
	Vec2f r = {cosf(dir), sinf(dir)};
	return r;
}

Vec2f vec2f_interp(Vec2f v0, Vec2f v1, float a1) {
	float a0 = 1.0f - a1;
	v0 = vec2f_scale(v0, a0);
	v1 = vec2f_scale(v1, a1);
	return vec2f_add(v0, v1);
}

Vec2f vec2f_inv(Vec2f v) {
	Vec2f r = {1.0f / v.x, 1.0f / v.y};
	return r;
}

Vec2f vec2f_rot90(Vec2f v) {
	Vec2f r = {-v.y, v.x};
	return r;
}

Vec2f vec2f_scale(Vec2f v, float s) {
	Vec2f r = {v.x * s, v.y * s};
	return r;
}

Vec2f vec2f_sub(Vec2f v0, Vec2f v1) {
	Vec2f r = {v0.x - v1.x, v0.y - v1.y};
	return r;
}

Vec2f vec2f_unit(Vec2f v) {
	return vec2f_scale(v, 1.0f / vec2f_dist(v));
}
