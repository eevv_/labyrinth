/*
log.h
-> handle logging of
   internal events
*/

#pragma once

void log_err(const char* format, ...);
void log_free(void);
void log_init(void);
void log_ok(const char* format, ...);
