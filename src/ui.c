#include "log.h"
#include "ui.h"

void ui_free(UI* ui) {
	tex_free(&ui->tex);
	text2d_free(&ui->text2d);
}

static const char* tex_path = "res/font.ff";
static const char* evnt_to_str[] = {
	"exit found",
	"door unlocked",
	"got key",
	"escape"
};

void ui_init(UI* ui) {
	log_ok("intializing ui");
	
	if (!tex_from_path(&ui->tex, tex_path)) {
		log_err("missing texture `%s`", tex_path);
	}
	
	Pnt pnt = pnt_from(vec2i_from(0, 0), vec2f_from(0.0f, 0.0f));
	ui->text2d = text2d_from(32, pnt, NULL);
}

void ui_refresh(UI* ui, const Scene* scene) {
	text2d_set_text(&ui->text2d, evnt_to_str[scene->ents.size]);
}

void ui_render(const UI* ui, Win* win) {
	text2d_render(&ui->text2d, win, &ui->tex);
}

void ui_update(UI* ui, const Scene* scene) {
	if (scene->evnt == EVNT_NONE) {
		return;
	}
	
	ui_refresh(ui, scene);
}
