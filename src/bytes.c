#include <assert.h>
#include <stdio.h>
#include "bytes.h"

void bytes_free(Bytes* bytes) {
	free(bytes->bytes);
}

bool bytes_from_path(Bytes* bytes, const char* path) {
	FILE* file = fopen(path, "rb");
	if (!file) {
		return false;
	}
	
	fseek(file, 0, SEEK_END);
	bytes->size = ftell(file);
	bytes->bytes = malloc(bytes->size * sizeof(uint8_t));
	fseek(file, 0, SEEK_SET);
	
	size_t size = fread(bytes->bytes, sizeof(uint8_t), bytes->size, file);
	fclose(file);
	if (size != bytes->size) {
		return false;
	}
	
	return true;
}

void bytes_init(Bytes* bytes, size_t size) {
	bytes->size = size;
	bytes->bytes = malloc(size * sizeof(uint8_t));
}

bool bytes_to_path(const Bytes* bytes, const char* path) {
	FILE* file = fopen(path, "w");
	if (!file) {
		return false;
	}
	
	size_t size = fwrite(bytes->bytes, sizeof(uint8_t), bytes->size, file);
	fclose(file);
	if (size != bytes->size) {
		return false;
	}
	
	return true;
}
