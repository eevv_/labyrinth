/*
ent.h
-> handling of entities
-> tracing entities (seeing if a ray hits)
*/

#pragma once

#include <stdbool.h>
#include <stdlib.h>
#include "axis.h"
#include "bytes.h"
#include "vec2f.h"

typedef enum {
	ENT_EXIT,
	ENT_DOOR,
	ENT_KEY
} EntType;

typedef struct {
	EntType type;
	Vec2f pos;
	/* only used when ent is ENT_DOOR */
	/* for simplicity sake there are only 2 directions */
	/* dir = false <-> dir = 0, 1 */
	/* dir = true <-> dir = 1, 0 */
	/* so vector projections are easier */
	Axis axis;
} Ent;

typedef struct {
	bool hit;
	EntType ent_type;
	float x;
	float dist;
} EntTrace;

Vec2f ent_door_dir(const Ent* ent);
EntTrace ent_trace(const Ent* ent, Vec2f pos, Vec2f dir);

typedef struct {
	size_t size;
	Ent* ents;
} Ents;

typedef struct {
	size_t size;
	EntTrace traces[3];
} EntsTrace;

void ents_free(Ents* ents);
Ent* ents_get_ptr(const Ents* ents, int i);
void ents_init(Ents* ents, size_t size);
EntsTrace ents_trace(const Ents* ents, Vec2f pos, Vec2f dir);
