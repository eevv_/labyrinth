/*
input.h
-> input definition
*/

#pragma once

#include <stdbool.h>

typedef struct {
	bool front;
	bool back;
	bool right;
	bool left;
	float offset_dir;
} Input;

bool input_no_move(const Input* input);
