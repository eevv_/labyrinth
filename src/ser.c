#include "ser.h"

bool ser_ent(Ent ent, size_t* pos, Bytes* bytes) {
	if (!ser_uint8(ent.type, pos, bytes)) {
		return false;
	}
	
	if (!ser_vec2f(ent.pos, pos, bytes)) {
		return false;
	}
	
	if (!ser_uint8(ent.axis, pos, bytes)) {
		return false;
	}
	
	return true;
}

bool ser_ents(Ents ents, size_t* pos, Bytes* bytes) {
	if (!ser_uint8(ents.size, pos, bytes)) {
		return false;
	}
	
	for (size_t i = 0; i < ents.size; i += 1) {
		if (!ser_ent(ents.ents[i], pos, bytes)) {
			return false;
		}
	}
	
	return true;
}

bool ser_float(float flt, size_t* pos, Bytes* bytes) {
	/* assumes float is 32 bit */
	union {
		uint32_t uint32;
		float flt;
	} b32;
	b32.flt = flt;
	return ser_uint32(b32.uint32, pos, bytes);
}

bool ser_map(Map map, size_t* pos, Bytes* bytes) {
	if (!ser_uint32(map.width, pos, bytes)) {
		return false;
	}
	
	if (!ser_uint32(map.height, pos, bytes)) {
		return false;
	}
	
	for (int i = 0; i < map.width * map.height; i += 1) {
		if (!ser_uint8(map.tiles[i], pos, bytes)) {
			return false;
		}
	}
	
	return true;
}

bool ser_player(Player player, size_t* pos, Bytes* bytes) {
	if (!ser_vec2f(player.pos, pos, bytes)) {
		return false;
	}
	
	if (!ser_float(player.dir, pos, bytes)) {
		return false;
	}
	
	return true;
}

bool ser_scene(Scene scene, size_t* pos, Bytes* bytes) {
	if (!ser_player(scene.player, pos, bytes)) {
		return false;
	}
	
	if (!ser_map(scene.map, pos, bytes)) {
		return false;
	}
	
	if (!ser_ents(scene.ents, pos, bytes)) {
		return false;
	}
	
	if (!ser_uint8(scene.evnt, pos, bytes)) {
		return false;
	}
	
	return true;
}

bool ser_uint8(uint8_t uint8, size_t* pos, Bytes* bytes) {
	if (*pos >= bytes->size) {
		return false;
	}
	
	bytes->bytes[*pos] = uint8;
	*pos += 1;
	
	return true;
}

bool ser_uint16(uint16_t uint16, size_t* pos, Bytes* bytes) {
	if (!ser_uint8(uint16 >> 8, pos, bytes)) {
		return false;
	}
	
	if (!ser_uint8(uint16 & 0xff, pos, bytes)) {
		return false;
	}
	
	return true;
}

bool ser_uint32(uint32_t uint32, size_t* pos, Bytes* bytes) {
	if (!ser_uint16(uint32 >> 16, pos, bytes)) {
		return false;
	}
	
	if (!ser_uint16(uint32 & 0xffff, pos, bytes)) {
		return false;
	}
	
	return true;
}

bool ser_uint64(uint64_t uint64, size_t* pos, Bytes* bytes) {
	if (!ser_uint32(uint64 >> 32, pos, bytes)) {
		return false;
	}
	
	if (!ser_uint32(uint64 & 0xffffffff, pos, bytes)) {
		return false;
	}
	
	return true;
}

bool ser_vec2f(Vec2f v, size_t* pos, Bytes* bytes) {
	if (!ser_float(v.x, pos, bytes)) {
		return false;
	}
	
	if (!ser_float(v.y, pos, bytes)) {
		return false;
	}
	
	return true;
}
