/*
cfg.h
-> configure various
   internal aspects of the game
*/

#pragma once

#include <stdbool.h>

/* IF YOU WANT TO KNOW */
/* WHY STUFF IS DEFINED HERE */
/* [WTF 0.1f IS NOT CONSTANT] */
/* `gcc -v` `gcc version 9.2.0 (GCC) ` */

/*
src/scene.c:114:32: error: initializer element is not constant
  114 | static const float padding_d = 0.1f - cfg_epsilon;
      |                                ^~~~
*/

/* if the game freezes randomly it is because */
/* epsilon was not small enough */
/* this is because the precision is broken when */
/* resolving the collision */
/* also higher values of epsilon will cause */
/* jittering effect when sliding across a surface */
/* TODO however it is fixable by caching whether */
/* the player has collided X/Y in this tick */
#define CFG_EPSILON 0.001f
#define CFG_LOG_TO_FILE true
