/*
map.h
-> map, tile, maptrace definition
-> used in (scene.*)
-> map generation
-> find a path between two points
-> find an area surrounding a point
*/

#pragma once

#include <stdbool.h>
#include "axis.h"
#include "bytes.h"
#include "vec2f.h"
#include "vec2i.h"

typedef enum {
	TILE_FLOOR,
	TILE_WALL
} Tile;

typedef struct {
	int width;
	int height;
	Tile* tiles;
} Map;

typedef struct {
	Tile tile;
	Vec2f pos;
	Vec2i tile_pos;
	Axis axis;
	float dist;
} MapTrace;

void map_clear(Map* map, Tile tile);
bool map_contains(const Map* map, Vec2i pos);
Vec2i* map_fill(const Map* map, Vec2i start, size_t* size);
void map_free(Map* map);
void map_gen(Map* map);
Tile map_get(const Map* map, Vec2i pos);
void map_init(Map* map, int w, int h);
Vec2i* map_path(const Map* map, Vec2i start, Vec2i end, size_t* size);
void map_set(Map* map, Vec2i pos, Tile tile);
MapTrace map_trace(const Map* map, Vec2f pos, Vec2f dir);
