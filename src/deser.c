#include <string.h>
#include "deser.h"

bool deser_ent(Ent* ent, size_t* pos, const Bytes* bytes) {
	uint8_t type;
	if (!deser_uint8(&type, pos, bytes)) {
		return false;
	}
	
	if (!deser_vec2f(&ent->pos, pos, bytes)) {
		return false;
	}
	
	uint8_t axis;
	if (!deser_uint8(&axis, pos, bytes)) {
		return false;
	}
	
	ent->type = type;
	ent->axis = axis;
	
	return true;
}

bool deser_ents(Ents* ents, size_t* pos, const Bytes* bytes) {
	uint8_t size;
	if (!deser_uint8(&size, pos, bytes)) {
		return false;
	}
	
	ents_init(ents, size);
	for (size_t i = 0; i < ents->size; i += 1) {
		if (!deser_ent(ents->ents + i, pos, bytes)) {
			return false;
		}
	}
	
	return true;
}

bool deser_farb(Farb* farb, size_t* pos, const Bytes* bytes) {
	/* magic number size */
	if (*pos + 7 >= bytes->size) {
		return false;
	}
	
	/* magic number */
	if (memcmp(bytes->bytes, "farbfeld", 8)) {
		return false;
	}
	*pos += 8;
	
	uint32_t width;
	if (!deser_uint32(&width, pos, bytes)) {
		return false;
	}
	
	uint32_t height;
	if (!deser_uint32(&height, pos, bytes)) {
		return false;
	}
	
	farb_init(farb, width, height);
	for (int i = 0; i < farb->width * farb->height; i += 1) {
		if (!deser_uint64(farb->pixs + i, pos, bytes)) {
			return false;
		}
	}
	
	return true;
}

bool deser_float(float* flt, size_t* pos, const Bytes* bytes) {
	return deser_uint32((uint32_t*) flt, pos, bytes);
}

bool deser_map(Map* map, size_t* pos, const Bytes* bytes) {
	uint32_t width;
	if (!deser_uint32(&width, pos, bytes)) {
		return false;
	}
	
	uint32_t height;
	if (!deser_uint32(&height, pos, bytes)) {
		return false;
	}
	
	map_free(map);
	map_init(map, width, height);
	
	for (size_t i = 0; i < width * height; i += 1) {
		uint8_t tile;
		if (!deser_uint8(&tile, pos, bytes)) {
			return false;
		}
		map->tiles[i] = tile;
	}
	
	return true;
}

bool deser_player(Player* player, size_t* pos, const Bytes* bytes) {
	if (!deser_vec2f(&player->pos, pos, bytes)) {
		return false;
	}
	
	if (!deser_float(&player->dir, pos, bytes)) {
		return false;
	}
	
	return true;
}

bool deser_scene(Scene* scene, size_t* pos, const Bytes* bytes) {
	if (!deser_player(&scene->player, pos, bytes)) {
		return false;
	}
	
	if (!deser_map(&scene->map, pos, bytes)) {
		return false;
	}
	
	if (!deser_ents(&scene->ents, pos, bytes)) {
		return false;
	}
	
	uint8_t evnt;
	if (!deser_uint8(&evnt, pos, bytes)) {
		return false;
	}
	
	scene->evnt = evnt;
	
	return true;
}

bool deser_uint8(uint8_t* out, size_t* pos, const Bytes* bytes) {
	if (*pos >= bytes->size) {
		return false;
	}
	
	*out = bytes->bytes[*pos];
	*pos += 1;
	
	return true;
}

bool deser_uint16(uint16_t* out, size_t* pos, const Bytes* bytes) {
	uint8_t uint8;
	if (!deser_uint8(&uint8, pos, bytes)) {
		return false;
	}
	
	*out = uint8;
	if (!deser_uint8(&uint8, pos, bytes)) {
		return false;
	}
	*out = (*out << 8) | uint8;
	
	return true;
}

bool deser_uint32(uint32_t* out, size_t* pos, const Bytes* bytes) {
	uint16_t uint16;
	if (!deser_uint16(&uint16, pos, bytes)) {
		return false;
	}
	
	*out = uint16;
	if (!deser_uint16(&uint16, pos, bytes)) {
		return false;
	}
	*out = (*out << 16) | uint16;
	
	return true;
}

bool deser_uint64(uint64_t* out, size_t* pos, const Bytes* bytes) {
	uint32_t uint32;
	if (!deser_uint32(&uint32, pos, bytes)) {
		return false;
	}
	
	*out = uint32;
	if (!deser_uint32(&uint32, pos, bytes)) {
		return false;
	}
	*out = (*out << 32) | uint32;
	
	return true;
}

bool deser_vec2f(Vec2f* vec2f, size_t* pos, const Bytes* bytes) {
	if (!deser_float(&vec2f->x, pos, bytes)) {
		return false;
	}
	
	if (!deser_float(&vec2f->y, pos, bytes)) {
		return false;
	}
	
	return true;
}
