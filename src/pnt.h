/*
pnt.h
-> point definition
-> a point is adaptive to
   screen resolution change
-> by storing offset and
   screen dimension coefficients
-> used in UI, 2d texts (ui.*, text2d.*)
*/

#pragma once

#include "vec2f.h"
#include "vec2i.h"

typedef struct {
	/* offset */
	Vec2i o;
	/* weight */
	Vec2f w;
} Pnt;

Pnt pnt_from(Vec2i o, Vec2f w);
Vec2i pnt_to_vec2i(Pnt p, Vec2i w);
