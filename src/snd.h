/*
snd.h
-> handles sound
-> very basic SDL2
   implementation
*/

#pragma once

#include <stdlib.h>
#include <SDL2/SDL.h>

typedef struct {
	Uint32 size;
	Uint8* wav;
	SDL_AudioSpec spec;
	
	Uint8* curr_wav;
	Uint32 curr_size;
} Snd;

void snd_free(Snd* snd);
void snd_init(Snd* snd);
