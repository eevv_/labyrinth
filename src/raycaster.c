#include <SDL2/SDL.h>
#include <assert.h>
#include <stdbool.h>
#include "log.h"
#include "pix.h"
#include "raycaster.h"
#include "tex.h"
#include "vec3f.h"

/* TODO */
void raycaster_free(Raycaster* raycaster) {
	texs_free(&raycaster->texs);
}

static const char* tex_paths[] = {
	/* exit, door, key */
	/* in that order so it matches with the */
	/* entity indices */
	"res/exit.ff",
	"res/door.ff",
	"res/key.ff",
	"res/floor.ff",
	"res/wall.ff",
	"res/ceil.ff",
};
static const size_t tex_paths_size = sizeof(tex_paths) / sizeof(const char*);

void raycaster_init(Raycaster* raycaster) {
	log_ok("initializing raycaster");
	int i = texs_from_paths(&raycaster->texs, tex_paths_size, tex_paths);
	if (i == -1) {
		return;
	}
	
	log_err("missing texture `%s`", tex_paths[i]);
}

Vec3f tile_to_color(Tile tile) {
	switch (tile) {
	case TILE_WALL:
		return vec3f_from(1.0f, 1.0f, 1.0f);
	default:
		assert(false);
	}
}

static void pix_set(Win* win, Vec2i pos, Vec3f color) {
	win_pix_set(win, pos, pix_from_vec3f(color));
}

/* TODO revise */
void raycaster_render_column(Raycaster* raycaster, Win* win, const Scene* scene, const MapTrace* trace, Vec2f dir, int x, int start, int end) {
	/* get texture x coordinate */
	Vec2f tex_wall_coord;
	/* have to flip textures on a per-axis basis */
	switch (trace->axis) {
	case AXIS_NX:
		tex_wall_coord.x = -trace->pos.y;
		break;
	case AXIS_PX:
		tex_wall_coord.x = trace->pos.y;
		break;
	case AXIS_NY:
		tex_wall_coord.x = trace->pos.x;
		break;
	case AXIS_PY:
		tex_wall_coord.x = -trace->pos.x;
		break;
	}
	
	/* y coordinate starts at top of the screen */
	float ray_z = 1.0f;
	float inc_z = -2.0f / win->height;
	
	Tex* tex_ceil = texs_get_ptr(&raycaster->texs, 5);
	Tex* tex_wall = texs_get_ptr(&raycaster->texs, 4);
	Tex* tex_floor = texs_get_ptr(&raycaster->texs, 3);
	
	/* FIXME figure out proper ray_z */
	/* with formulas and not guess work */
	/* ESPECIALLY the constant 0.665 part */
	/* TODO split into 3 loops */
	/* TODO find the y texture bug */
	Vec3f color;
	for (int y = 0; y < win->height; y += 1) {
		float dist;
		if (y < start) {
			/* ceil tracing */
			/* TODO use lookup table */
			dist = win->aspect / ray_z; //(float) win->height / (float) (win->height - 2 * y);
			float interp = dist / (trace->dist * 2.0f);
			Vec2f tex_ceil_coord = vec2f_interp(scene->player.pos, trace->pos, interp);
			color = tex_sample(tex_ceil, tex_ceil_coord);
			color = vec3f_scale(color, 1.0f / (dist*dist + 1.0f));
			color = vec3f_clamp(color, 0.0f, 1.0f);
			pix_set(win, vec2i_from(x, y), color);
		} else if (y > end) {
			/* floor tracing */
			dist = -win->aspect / ray_z; //(float) win->height / (float) (2 * y - win->height);
			float interp = dist / (trace->dist * 2.0f);
			Vec2f tex_floor_coord = vec2f_interp(scene->player.pos, trace->pos, interp);
			color = tex_sample(tex_floor, tex_floor_coord);
			color = vec3f_scale(color, 1.0f / (dist*dist + 1.0f));
			color = vec3f_clamp(color, 0.0f, 1.0f);
			pix_set(win, vec2i_from(x, y), color);
		} else {
			/* wall tracing */
			tex_wall_coord.y = (float) (y - start) / (float) (end - start + 1);	
			color = tex_sample(tex_wall, tex_wall_coord);
			color = vec3f_scale(color, 1.0f / (trace->dist*trace->dist + 1.0f));
			color = vec3f_clamp(color, 0.0f, 1.0f);
			pix_set(win, vec2i_from(x, y), color);
		}
		ray_z += inc_z;
	}
}

/* TODO revise */
static void render_ent(Raycaster* raycaster, Win* win, const EntTrace* trace, int x, float* curr_dist) {
	if (!trace->hit) {
		return;
	}
	
	if (*curr_dist < trace->dist) {
		return;
	}
	*curr_dist = trace->dist;
	
	int column = (float) win->width / trace->dist;
	column /= 4;
	
	int start = win->height / 2 - column;
	int end = win->height / 2 + column;
	
	Vec2f tex_ent_coord;
	tex_ent_coord.x = trace->x + 0.5f;
	Vec3f color;
	for (int y = 0; y < win->height; y += 1) {
		if (y >= start && y <= end) {
			tex_ent_coord.y = (float) (y - start) / (float) (end - start + 1);
			/* ent_type is used as an index into textures since it is one of 0, 1, 2 */
			Tex* tex = texs_get_ptr(&raycaster->texs, trace->ent_type);
			color = tex_sample(tex, tex_ent_coord);
			if (color.x + color.y + color.z == 0.0f) {
				continue;
			}
			pix_set(win, vec2i_from(x, y), color);
		}
	}
}

/* TODO revise */
/* `col` is for depth testing */
static void render_ents(Raycaster* raycaster, Win* win, const EntsTrace* trace, int x, float dist) {
	for (int i = 0; i < trace->size; i += 1) {
		render_ent(raycaster, win, trace->traces + i, x, &dist);
	}
}

/* TODO make nicer */
void raycaster_render(Raycaster* raycaster, Win* win, const Scene* scene) {
	float width_half = (float) win->width;
	width_half *= 0.5f;

	Vec2f dir = vec2f_from_dir(scene->player.dir + M_PI / 2.0f);
	/* column width (increment amount) */
	Vec2f inc;
	inc.x = -dir.x;
	inc.y = -dir.y;
	inc = vec2f_scale(inc, 2.0f / (float) win->width);
	dir = vec2f_add(dir, vec2f_from_dir(scene->player.dir));

	/* render column-by-column */
	for (int x = 0; x < win->width; x += 1) {
		MapTrace trace = map_trace(&scene->map, scene->player.pos, dir);
		/* `(float) win->width / [...]` here for aspect ratio fix */
		int column = (float) win->width / trace.dist;
		column /= 4;
		int start = win->height / 2 - column;
		int end = win->height / 2 + column;
		raycaster_render_column(raycaster, win, scene, &trace, dir, x, start, end);
		
		EntsTrace e_trace = ents_trace(&scene->ents, scene->player.pos, dir);
		render_ents(raycaster, win, &e_trace, x, trace.dist);
		
		dir = vec2f_add(dir, inc);
	}
}
