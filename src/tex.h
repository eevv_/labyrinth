/*
texture.h
-> handles textures
-> converting from file
   to internal format
-> sampling textures
-> used in the raycaster (raycaster.*)
-> used in the UI (ui.*)
-> used for 2d text (text2d.*)
*/

#pragma once

#include <stdbool.h>
#include "vec2f.h"
#include "vec2i.h"
#include "vec3f.h"

/* texture.h
stuff about textures
extracting colors from textures
*/

/* texture format */
/* TODO consider putting inverses of wf, hf */
typedef struct {
	int w;
	int h;

	/* for use in sampling */
	/* just width and height but in float */
	float wf;
	float hf;
	
	/* best to read from sequential y values */
	/* (as the column rendering does) */
	/* """cache-friendly""" */
	Vec3f* colors;
} Tex;

void tex_free(Tex* tex);
bool tex_from_path(Tex* tex, const char* path);
Vec3f tex_get(const Tex* tex, Vec2i pos);
void tex_init(Tex* tex, int w, int h);
/* the sampling is wrapping */
Vec3f tex_sample(const Tex* tex, Vec2f p);
void tex_set(Tex* tex, Vec2i pos, Vec3f color);

/* textures */
typedef struct {
	size_t size;
	Tex* texs;
} Texs;

void texs_free(Texs* texs);
/* returns -1 if no error */
/* otherwise returns an index into paths */
int texs_from_paths(Texs* texs, size_t size, const char** paths);
Tex* texs_get_ptr(Texs* texs, int i);
