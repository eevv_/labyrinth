/*
deser.h
-> deserialization for
   various types
*/

#pragma once

#include <stdbool.h>
#include <stdint.h>
#include "bytes.h"
#include "farb.h"
#include "scene.h"

bool deser_ent(Ent* ent, size_t* pos, const Bytes* bytes);
bool deser_ents(Ents* ents, size_t* pos, const Bytes* bytes);
bool deser_farb(Farb* farb, size_t* pos, const Bytes* bytes);
bool deser_float(float* flt, size_t* pos, const Bytes* bytes);
bool deser_map(Map* map, size_t* pos, const Bytes* bytes);
bool deser_player(Player* player, size_t* pos, const Bytes* bytes);
bool deser_scene(Scene* scene, size_t* pos, const Bytes* bytes);
bool deser_uint8(uint8_t* out, size_t* pos, const Bytes* bytes);
bool deser_uint16(uint16_t* out, size_t* pos, const Bytes* bytes);
bool deser_uint32(uint32_t* out, size_t* pos, const Bytes* bytes);
bool deser_uint64(uint64_t* out, size_t* pos, const Bytes* bytes);
bool deser_vec2f(Vec2f* vec2f, size_t* pos, const Bytes* bytes);
