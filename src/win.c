#include "win.h"

void win_free(Win* win) {
	SDL_DestroyWindow(win->sdl_win);
}

void win_init(Win* win, const char* title, int width, int height) {
	win->width = width;
	win->height = height;
	win->aspect = (float) width / (float) height;

	SDL_Window* sdl_win = SDL_CreateWindow(
		title,
		SDL_WINDOWPOS_UNDEFINED,
		SDL_WINDOWPOS_UNDEFINED,
		width,
		height,
		0
	);
	win->sdl_win = sdl_win;

	SDL_Surface* sdl_surf = SDL_GetWindowSurface(sdl_win);
	win->sdl_surf = sdl_surf;
	SDL_FillRect(sdl_surf, 0, 0);
	/* `win_update(win);` */
	SDL_UpdateWindowSurface(sdl_win);
}

/* http://sdl.beuc.net/sdl.wiki/Pixel_Access */
void win_pix_set(Win* win, Vec2i pos, Uint32 color) {
	SDL_Surface* sdl_surf = win->sdl_surf;
	int bpp = sdl_surf->format->BytesPerPixel;
	Uint8* ptr_pix = (Uint8*) sdl_surf->pixels + pos.y * sdl_surf->pitch + pos.x * bpp;
	
	switch (bpp) {
	case 1:
		*ptr_pix = color;
		return;
	case 2:
		*(Uint16*) ptr_pix = color;
		return;
	case 3:
		if (SDL_BYTEORDER == SDL_BIG_ENDIAN) {
			ptr_pix[0] = (color >> 16) & 0xff;
			ptr_pix[1] = (color >> 8) & 0xff;
			ptr_pix[2] = color & 0xff;
		} else {
			ptr_pix[0] = color & 0xff;
			ptr_pix[1] = (color >> 8) & 0xff;
			ptr_pix[2] = (color >> 16) & 0xff;
		}
		return;
	case 4:
		*(Uint32*) ptr_pix = color;
	}
}

void win_update(Win* win) {
	SDL_UpdateWindowSurface(win->sdl_win);
}
