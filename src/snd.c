#include "log.h"
#include "snd.h"

void snd_free(Snd* snd) {
	SDL_CloseAudio();
	//SDL_FreeWAV(snd->wav);
}

static void audio_callback(void* userdata, Uint8* wav, int size) {
	Snd* snd = userdata;
	
	if (snd->size == 0) {
		return;
	}
	
	size = (size > snd->size) ? snd->size : size;
	SDL_memcpy(wav, snd->wav, size);
	
	snd->wav += size;
	snd->size -= size;
}

void snd_init(Snd* snd) {
	log_ok("initializing sound");
	
	log_ok("-> (sound) \"res/music.wav\"");
	if (!SDL_LoadWAV("res/music.wav", &snd->spec, &snd->wav, &snd->size)) {
		log_err("failed to load audio `res/music.wav`");
	}
	
	snd->spec.callback = audio_callback;
	snd->spec.userdata = snd;
	
	snd->curr_wav = snd->wav;
	snd->curr_size = snd->size;
	
	if (SDL_OpenAudio(&snd->spec, NULL)) {
		log_err("SDL2 open audio fail");
	}
	
	SDL_PauseAudio(0);
}
