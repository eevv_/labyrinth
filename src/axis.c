#include "axis.h"

bool axis_is_x(Axis axis) {
	return axis == AXIS_NX || axis == AXIS_PX;
}

bool axis_is_y(Axis axis) {
	return axis == AXIS_NY || axis == AXIS_PY;
}

Axis axis_rot90(Axis axis) {
	return (axis + 1) % 4;
}

Vec2f axis_to_vec2f(Axis axis) {
	Vec2f r;
	switch (axis) {
	case AXIS_PX:
		r = vec2f_from(1.0f, 0.0f);
		break;
	case AXIS_PY:
		r = vec2f_from(0.0f, 1.0f);
		break;
	case AXIS_NX:
		r = vec2f_from(-1.0f, 0.0f);
		break;
	case AXIS_NY:
		r = vec2f_from(0.0f, -1.0f);
	}
	return r;
}
