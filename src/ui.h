/*
ui.h
-> handles the ui
   (currently only one 2d text)
*/

#pragma once

#include "scene.h"
#include "tex.h"
#include "text2d.h"
#include "win.h"

typedef struct {
	/* there is only one font */
	/* so there needs to be only */
	/* one texture */
	Tex tex;
	/* only need one (for now?) */
	Text2D text2d;
} UI;

void ui_free(UI* ui);
void ui_init(UI* ui);
void ui_refresh(UI* ui, const Scene* scene);
void ui_render(const UI* ui, Win* win);
/* the render and update is split */
/* because I might detach the */
/* rendering side from the update */
/* e.g. rendering runs at unlimited fps */
/* while updates happen 20 times per second */
void ui_update(UI* ui, const Scene* scene);
