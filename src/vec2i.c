#include "vec2i.h"

Vec2i vec2i_add(Vec2i v0, Vec2i v1) {
	Vec2i r = {v0.x + v1.x, v0.y + v1.y};
	return r;
}

static int clamp(int v, int s, int e) {
	if (v < s) {
		return v;
	}

	if (v > e) {
		return e;
	}

	return v;
}

Vec2i vec2i_clamp(Vec2i v, float s, float e) {
	Vec2i r;
	r.x = clamp(v.x, s, e);
	r.y = clamp(v.y, s, e);
	return r;
}

Vec2i vec2i_div(Vec2i v, int d) {
	Vec2i r = {v.x / d, v.y / d};
	return r;
}

Vec2i vec2i_from(int x, int y) {
	Vec2i r = {x, y};
	return r;
}

Vec2i vec2i_sub(Vec2i v0, Vec2i v1) {
	Vec2i r = {v0.x - v1.x, v0.y - v1.y};
	return r;
}
