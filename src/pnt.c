#include "pnt.h"

Pnt pnt_from(Vec2i o, Vec2f w) {
	Pnt p = {o, w};
	return p;
}

Vec2i pnt_to_vec2i(Pnt p, Vec2i w) {
	Vec2i r = {p.w.x * w.x, p.w.y * w.y};
	return vec2i_add(r, p.o);
}
