#include "vec3f.h"

/* TODO add this in a seperate file */
static float clamp(float v, float s, float e) {
	float min = (v > s) ? v : s;
	return (min < e) ? min : e;
}

Vec3f vec3f_clamp(Vec3f v, float s, float e) {
	Vec3f r;
	r.x = clamp(v.x, s, e);
	r.y = clamp(v.y, s, e);
	r.z = clamp(v.z, s, e);
	return r;
}

Vec3f vec3f_from(float x, float y, float z) {
	Vec3f v = {x, y, z};
	return v;
}

/* TODO add this in a seperate file */
float inv_interp(float s, float e, float p) {
	float to = e - s;
	if (to == 0) {
		/* doesn't matter what you return */
		return 0.0f;
	}
	p -= s;
	return p / to;
}

float vec3f_inv_interp(Vec3f s, Vec3f e, Vec3f p) {
	p.x = inv_interp(s.x, e.x, p.x);
	p.y = inv_interp(s.y, e.y, p.y);
	p.z = inv_interp(s.z, e.z, p.z);
	return (p.x + p.y + p.z) / 3.0f;
}

Vec3f vec3f_scale(Vec3f v, float s) {
	Vec3f r = {v.x * s, v.y * s, v.z * s};
	return r;
}
