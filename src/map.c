#include <assert.h>
#include <math.h>
#include <stdbool.h>
#include <stdlib.h>
#include "deser.h"
#include "map.h"
#include "ser.h"

bool tile_solid(Tile tile) {
	return tile == TILE_WALL;
}

void map_clear(Map* map, Tile tile) {
	for (int i = 0; i < map->width * map->height; i += 1) {
		map->tiles[i] = tile;
	}
}

bool map_contains(const Map* map, Vec2i pos) {
	if (pos.x < 0 || pos.x >= map->width || pos.y < 0 || pos.y >= map->height) {
		return false;
	}

	return true;
}

/* map generation stuff */
/* map generation uses a stack of points */
typedef struct {
	size_t size;
	Vec2i* data;
} Stack;

static bool stack_empty(Stack* stack) {
	return stack->size == 0;
}

static void stack_free(Stack* stack) {
	free(stack->data);
}

static void stack_init(Stack* stack) {
	stack->size = 0;
	stack->data = NULL;
}

static void stack_pop(Stack* stack) {
	assert(stack->size > 0);
	stack->size -= 1;
	stack->data = realloc(stack->data, stack->size * sizeof(Vec2i));
}

static void stack_push(Stack* stack, Vec2i v) {
	stack->size += 1;
	stack->data = realloc(stack->data, stack->size * sizeof(Vec2i));
	stack->data[stack->size - 1] = v;
}

static Vec2i stack_top(Stack* stack) {
	assert(stack->size > 0);
	return stack->data[stack->size - 1];
}

static Vec2i vec2is_get(Vec2i* vec2is, int width, Vec2i pos) {
	return vec2is[pos.x + pos.y * width];
}

static void vec2is_set(Vec2i* vec2is, int width, Vec2i pos, Vec2i val) {
	vec2is[pos.x + pos.y * width] = val;
}

static bool map_fill_is_valid(const Map* map, bool* fills, Vec2i pos) {
	if (!map_contains(map, pos)) {
		return false;
	}
	
	if (map_get(map, pos) == TILE_WALL) {
		return false;
	}
	
	bool filled = fills[pos.x + pos.y * map->width];
	return !filled;	
}

static void map_fill_walk(const Map* map, Stack* stack, bool* fills) {
	Vec2i this = stack_top(stack);
	stack_pop(stack);
	
	fills[this.x + this.y * map->width] = true;
	
	Vec2i nexts[4] = {
		vec2i_add(this, vec2i_from(1, 0)),
		vec2i_add(this, vec2i_from(0, 1)),
		vec2i_add(this, vec2i_from(-1, 0)),
		vec2i_add(this, vec2i_from(0, -1))
	};
	
	for (int i = 0; i < 4; i += 1) {
		Vec2i next = nexts[i];
		if (!map_fill_is_valid(map, fills, next)) {
			continue;
		}
			
		stack_push(stack, next);
	}
}

Vec2i* map_fill(const Map* map, Vec2i start, size_t* size) {
	Stack stack;
	stack_init(&stack);
	stack_push(&stack, start);
	
	size_t fills_size = map->width * map->height;
	bool* fills = malloc(fills_size * sizeof(bool));
	
	for (size_t i = 0; i < fills_size; i += 1) {
		fills[i] = false;
	}
	
	do {
		map_fill_walk(map, &stack, fills);
	} while(!stack_empty(&stack));
	
	stack_free(&stack);
	
	Vec2i* vec2is = malloc(fills_size * sizeof(Vec2i));
	size_t pos = 0;
	for (size_t x = 0; x < map->width; x += 1) {
		for (size_t y = 0; y < map->height; y += 1) {
			if (fills[x + map->width * y]) {
				vec2is[pos] = vec2i_from(x, y);
				pos += 1;
			}
		}
	}
	
	*size = pos;	
	return vec2is;
}

void map_free(Map* map) {
	free(map->tiles);
}

/* check to see if the position is a valid branch target */
static bool map_gen_is_valid(const Map* map, Vec2i pos) {
	return map_contains(map, pos) && tile_solid(map_get(map, pos));
}

static void map_gen_pave(Map* map, Stack* stack, Vec2i pos, Vec2i dir) {
	Vec2i new_pos = vec2i_add(pos, dir);
	assert(map_gen_is_valid(map, new_pos));
	
	/* push the new position to later walk through it */
	stack_push(stack, new_pos);
	
	map_set(map, new_pos, TILE_FLOOR);
	
	/* have to create a path between two nodes */
	dir = vec2i_div(dir, 2);
	new_pos = vec2i_add(pos, dir);
	map_set(map, new_pos, TILE_FLOOR);
}

/* find the next point from this point */
static void map_gen_walk(Map* map, Stack* stack) {
	Vec2i this = stack_top(stack);
	/* 4 directions */
	Vec2i dirs[4] = {
		vec2i_from(2, 0),
		vec2i_from(0, 2),
		vec2i_from(-2, 0),
		vec2i_from(0, -2)
	};
	
	/* 4 direction validity checkers */
	bool dv[4];
	for (int i = 0; i < 4; i += 1) {
		Vec2i next = vec2i_add(this, dirs[i]);
		dv[i] = map_gen_is_valid(map, next);
	}
	
	/* number of valid branches */
	int vn = 0;
	int branches[4];
	for (int i = 0; i < 4; i += 1) {
		if (!dv[i]) {
			continue;
		}
		branches[vn] = i;
		vn += 1;
	}
	/* dead if there are no possible branches */
	bool dead = vn == 0;
	if (dead) {
		/* dead-end means this point is finished */
		stack_pop(stack);
		return;
	}
	
	int branch = branches[rand() % vn];
	
	/* pave the path with the corresponding branch direction */
	map_gen_pave(map, stack, this, dirs[branch]);
}

void map_gen(Map* map) {
	/* clear the map with walls */
	/* because the maze generator */
	/* places floors */
	map_clear(map, TILE_WALL);
	/* this single omitted line is responsible */
	/* for a lot of headaches */
	/* because it created a muli-path labyrinth */
	map_set(map, vec2i_from(0, 0), TILE_FLOOR);
	
	Stack stack;
	stack_init(&stack);
	/* set the starting position of the maze */
	stack_push(&stack, vec2i_from(0, 0));
	do {
		map_gen_walk(map, &stack);
	} while (!stack_empty(&stack));
	/* when the stack is empty, all nodes are filled */
	stack_free(&stack);
}

Tile map_get(const Map* map, Vec2i pos) {
	if (!map_contains(map, pos)) {
		return TILE_WALL;
	}

	return map->tiles[pos.x + pos.y * map->width];
}

void map_init(Map* map, int width, int height) {
	map->width = width;
	map->height = height;
	map->tiles = malloc(width * height * sizeof(Tile));
}

static bool map_path_is_valid(const Map* map, Vec2i* vec2is, Vec2i pos, Vec2i start) {
	if (!map_contains(map, pos)) {
		return false;
	}
	
	if (pos.x == start.x && pos.y == start.y) {
		return false;
	}
		
	Vec2i that = vec2is_get(vec2is, map->width, pos);
	if (that.x != pos.x || that.y != pos.y) {
		return false;
	}
	
	return map_get(map, pos) == TILE_FLOOR;
}

static bool map_path_walk(const Map* map, Stack* stack, Vec2i* vec2is, Vec2i start, Vec2i end) {
	Vec2i this = stack_top(stack);
	stack_pop(stack);
	
	if (this.x == end.x && this.y == end.y) {
		return true;
	}
	
	Vec2i nexts[4] = {
		vec2i_add(this, vec2i_from(1, 0)),
		vec2i_add(this, vec2i_from(0, 1)),
		vec2i_add(this, vec2i_from(-1, 0)),
		vec2i_add(this, vec2i_from(0, -1))
	};
	
	for (int i = 0; i < 4; i += 1) {
		Vec2i next = nexts[i];
		if (!map_path_is_valid(map, vec2is, next, start)) {
			continue;
		}
		
		vec2is_set(vec2is, map->width, next, this);
		stack_push(stack, next);
	}
	
	return false;
}

Vec2i* map_path(const Map* map, Vec2i start, Vec2i end, size_t* size) {
	assert(map_contains(map, start));
	assert(map_contains(map, end));
	
	Stack stack;
	stack_init(&stack);
	stack_push(&stack, start);
	
	size_t vec2is_size = map->width * map->height;
	Vec2i* vec2is = malloc(vec2is_size * sizeof(Vec2i));
	for (int x = 0; x < map->width; x += 1) {
		for (int y = 0; y < map->height; y += 1) {
			Vec2i vec = vec2i_from(x, y);
			vec2is_set(vec2is, map->width, vec, vec);
		}
	}
	
	while (!map_path_walk(map, &stack, vec2is, start, end));
	stack_free(&stack);
	
	Vec2i* ret = malloc(vec2is_size * sizeof(Vec2i));
	size_t pos = 0;
	
	while (end.x != start.x || end.y != start.y) {
		ret[pos] = end;
		pos += 1;
		end = vec2is_get(vec2is, map->width, end);
	}
	
	*size = pos;
	return ret;
}

void map_set(Map* map, Vec2i pos, Tile tile) {
	assert(map_contains(map, pos));
	map->tiles[pos.x + pos.y * map->width] = tile;
}

// TODO use float->int casting
MapTrace map_trace(const Map* map, Vec2f pos, Vec2f dir) {
	MapTrace trace;
	trace.pos = pos;
	trace.tile_pos.x = (int) floorf(pos.x);
	trace.tile_pos.y = (int) floorf(pos.y);
	Vec2f delta = vec2f_inv(dir);
	int step_x;
	int step_y;
	
	/* x calc */
	Axis axis_x;
	float dist_x;
	if (delta.x > 0.0f) {
		axis_x = AXIS_NX;
		dist_x = trace.tile_pos.x - pos.x + 1.0f;
		step_x = 1;
	} else {
		axis_x = AXIS_PX;
		dist_x = pos.x - trace.tile_pos.x;
		step_x = -1;
		delta.x = -delta.x;
	}
	dist_x *= delta.x;
	
	/* y calc */
	Axis axis_y;
	float dist_y;
	if (delta.y > 0.0f) {
		axis_y = AXIS_NY;
		dist_y = trace.tile_pos.y - pos.y + 1.0f;
		step_y = 1;
	} else {
		axis_y = AXIS_PY;
		dist_y = pos.y - trace.tile_pos.y;
		step_y = -1;
		delta.y = -delta.y;
	}
	dist_y *= delta.y;
	

	trace.dist = 0.0f;
	while (1) {
		trace.tile = map_get(map, trace.tile_pos);
		if (tile_solid(trace.tile)) {
			break;
		}
		if (dist_x < dist_y) {
			trace.dist = dist_x;
			dist_x += delta.x;
			trace.tile_pos.x += step_x;
			trace.axis = axis_x;
		} else {
			trace.dist = dist_y;
			dist_y += delta.y;
			trace.tile_pos.y += step_y;
			trace.axis = axis_y;
		}
	}
	dir = vec2f_scale(dir, trace.dist);
	trace.pos = vec2f_add(trace.pos, dir);
	return trace;
}
