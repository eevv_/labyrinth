#include <assert.h>
#include "deser.h"
#include "farb.h"

void farb_free(Farb* farb) {
	free(farb->pixs);
}

bool farb_from_path(Farb* farb, const char* path) {
	Bytes bytes;
	if (!bytes_from_path(&bytes, path)) {
		return false;
	}
	
	/* new file means offset pos = 0 */
	size_t pos = 0;
	if (!deser_farb(farb, &pos, &bytes)) {
		return false;
	}
	bytes_free(&bytes);
	
	return true;
}

uint64_t farb_get(const Farb* farb, Vec2i pos) {
	assert(pos.x >= 0 && pos.x < farb->width);
	assert(pos.y >= 0 && pos.y < farb->height);
	
	return farb->pixs[pos.x + pos.y * farb->width];
}

void farb_init(Farb* farb, uint32_t width, uint32_t height) {
	farb->width = width;
	farb->height = height;
	farb->pixs = malloc(width * height * sizeof(uint64_t));
}
