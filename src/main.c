#include <assert.h>
#include <SDL2/SDL.h>
#include <stdio.h>
#include <time.h>
#include "deser.h"	
#include "ent.h"
#include "log.h"
#include "map.h"
#include "player.h"
#include "raycaster.h"
#include "ser.h"
#include "snd.h"
#include "ui.h"
#include "vec2f.h"
#include "win.h"

float get_time() {
	float time = (float) SDL_GetTicks();
	return time / 1000.0f;
}

static const char* help_str = "\
usage:\n\
-> labyrinth new [width] [height]\n\
-> labyrinth load [path]\n\
where\n\
-> [width], [height] are integers in [2; infty)\n\
-> [path] is a valid path to a save file\n\
";

void show_help(void) {
	fputs(help_str, stdout);
	exit(1);
}

void err_cmd(const char* err_str) {
	printf("labyrinth: %s\n", err_str);
	exit(1);
}

void cmd_new(int args_size, const char** args, Scene* scene) {
	if (args_size != 4) {
		show_help();
	}
	
	int width;
	int scanned = sscanf(args[2], "%i", &width);
	if (scanned != 1 || width <= 1) {
		show_help();
	}
	
	int height;
	scanned = sscanf(args[3], "%i", &height);
	if (scanned != 1 || height <= 1) {
		show_help();
	}
	
	width = width * 2 - 1;
	height = height * 2 - 1;
	
	srand(time(NULL));
	scene_init(scene, width, height);
}

void cmd_load(int args_size, const char** args, Scene* scene) {
	if (args_size != 3) {
		show_help();
	}
	
	const char* path = args[2];
	Bytes bytes;
	if (!bytes_from_path(&bytes, path)) {
		err_cmd("invalid path");
	}
	
	size_t pos = 0;	
	if (!deser_scene(scene, &pos, &bytes)) {
		err_cmd("invalid save file");
	}
}

int main(int args_size, const char** args) {
	if (args_size <= 1) {
		show_help();
	}
	
	log_init();
	
	Scene scene;
	
	if (!strcmp(args[1], "new")) {
		cmd_new(args_size, args, &scene);
	} else if (!strcmp(args[1], "load")) {
		cmd_load(args_size, args, &scene);
	} else {
		show_help();
	}
	
	float tps = 30.0f;
	float spt = 1.0f / tps;
		
	/* ui */
	UI ui;
	ui_init(&ui);
	ui_refresh(&ui, &scene);
		
	/* renderer */
	Raycaster raycaster;
	raycaster_init(&raycaster);
	
	if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_AUDIO)) {
		log_err("SDL2 init video fail");
	}
	SDL_ShowCursor(SDL_DISABLE);
	
	/* sound */
	Snd snd;
	snd_init(&snd);
	
	/* window */
	Win win;
	win_init(&win, "labyrinth", 640, 480);
	
	bool quit = false;
	float time = 0.0f;
	float time_old = get_time();
	float time_now = time_old;
	
	const Uint8* state = SDL_GetKeyboardState(NULL);
	bool key_e = false;
	bool complete = false;
	
	SDL_Event event;
	while (!quit) {
		time_old = time_now;
		time_now = get_time();
		float dt = time_now - time_old;
		
		if (time > spt) {
			/* tick happens */
			time -= spt;
		} else {
			time += dt;
			continue;
		}

		while (SDL_PollEvent(&event)) {
			switch (event.type) {
			case SDL_QUIT:
				quit = true;
				break;
			case SDL_WINDOWEVENT_SIZE_CHANGED:
				/* TODO allow resize window and respond to size changed */
				break;
			default:
				break;
			}
		}
		
		if (state[SDL_SCANCODE_E]) {
			if (!key_e) {
				key_e = true;
				scene_fwrite(&scene, stdout);
			}
		} else {
			key_e = false;
		}
		
		Input input;	
		input.front = state[SDL_SCANCODE_W];
		input.back = state[SDL_SCANCODE_S];
		input.right = state[SDL_SCANCODE_D];
		input.left = state[SDL_SCANCODE_A];
		/* TODO add offset_dir (maybe?) */
		input.offset_dir = 0.0f;
		
		/*
		spt as dt
		because seconds per ticks
		is the delta time
		*/
		scene_update(&scene, spt, &input);
		if (scene.evnt == EVNT_EXIT) {
			log_ok("game complete");
			complete = true;
			break;
		}
		ui_update(&ui, &scene);
		raycaster_render(&raycaster, &win, &scene);
		ui_render(&ui, &win);
		win_update(&win);
		//printf("%2.2f %2.2f\n", scene.player.pos.x, scene.player.pos.y);
	}
	
	if (!complete) {
		log_ok("saving");
		log_ok("-> serialize");
		Bytes bytes;
		bytes_init(&bytes, 1024);
		size_t pos = 0;
		assert(ser_scene(scene, &pos, &bytes));
		bytes.size = pos;
		log_ok("-> write");
		if (!bytes_to_path(&bytes, "save.bin")) {
		/* TODO create log_bad */
		/* to replace */
			log_err("failed to write to \"save.bin\"");
		}
	}
		
	log_ok("freeing resources");
	win_free(&win);
	scene_free(&scene);
	snd_free(&snd);
	raycaster_free(&raycaster);
	ui_free(&ui);
	log_free();
	SDL_Quit();
	
	return 0;
}
