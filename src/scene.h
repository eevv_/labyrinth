/*
scene.h
-> handles game logic
-> provides some simple
   debugging/cheating
   with `scene_fwrite(...)`
*/

#pragma once

#include <stdio.h>
#include "bytes.h"
#include "ent.h"
#include "input.h"
#include "map.h"
#include "player.h"

/* scene.h
handles gamelogic
*/

/* events for different subgoals */
/* TODO revise if `EVNT_NONE` is needed */
typedef enum {
	EVNT_NONE,
	EVNT_KEY,
	EVNT_DOOR,
	EVNT_EXIT
} Evnt;

typedef struct {
	Player player;
	Map map;
	/* entities amount
	0 => exit
	1 => key
	2 => door
	*/
	//Ent ents[3];
	/* starts of at 3 */
	/* but gets decremented after */
	/* every subgoal */
	//size_t ents_size;
	Ents ents;
	
	/* events */
	Evnt evnt;
} Scene;

void scene_free(Scene* scene);
void scene_fwrite(const Scene* scene, FILE* file);
void scene_init(Scene* scene, int width, int height);
void scene_update(Scene* scene, float dt, const Input* input);
