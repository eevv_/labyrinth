/*
pix.h
-> handle Vec3f to u32 pixel
   conversions
-> mostly for toying around
   with dithering
*/

#pragma once

#include <SDL2/SDL.h>
#include "vec2i.h"
#include "vec3f.h"

Uint32 pix_8bit(Vec3f v);
Uint32 pix_from(int r, int g, int b);
Uint32 pix_from_vec3f(Vec3f v);
Uint32 pix_dither(Vec3f v, Vec2i p);
