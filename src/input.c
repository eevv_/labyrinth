#include "input.h"

bool input_no_move(const Input* input) {
	return !input->front && !input->back && !input->right && !input->left;
}
