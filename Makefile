objects=src/axis.o src/bytes.o src/deser.o src/ent.o src/farb.o src/input.o src/log.o src/main.o src/map.o src/pix.o src/pnt.o src/raycaster.o src/scene.o src/ser.o src/snd.o src/tex.o src/text2d.o src/ui.o src/vec2i.o src/vec2f.o src/vec3f.o src/win.o

CFLAGS=-O3 -Wall
LDFLAGS=-lSDL2 -lm

all: $(objects)
	$(CC) $(CFGLAGS) -o labyrinth $(objects) $(LDFLAGS)

clean:
	rm -f labyrinth $(objects)
