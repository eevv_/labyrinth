The game uses the [FF format](https://tools.suckless.org/farbfeld/), which is simple to decode. 
There are `png2ff` and `ff2png` programs for conversion.
Note: for entity textures the black pixels (R+G+B = 0) are transparent in-game.
# TODO
* Use a custom texture format
